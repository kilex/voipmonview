/**
 * Created by kilex on 7.05.15.
 */


$( document ).ready(function() {
                console.log( "ready!" );

    $(".help").popover({
        trigger: "hover",
        placement: "bottom",
        html:"true",
        delay: { show: 1000, hide: 200 }
    });

    $('.datepicker').datepicker();

    $.ajax({
        url: "ips.php",
        type: "GET"
    })
        .done(function( data ) {
            if ( console && console.log ) {
                $("#fulllist").html(data);
                setclicks();
            }
        });


    setfilter();

    $('.debug').each(function(){
        var debugid=$(this).data('id');
        //alert(debugid);
        $.ajax({
            url: "debug.php",
            type: "POST",
            data: { id:debugid }
        })
            .done(function( data ) {
                if ( console && console.log ) {
                    $("#"+debugid).html(data);
                    tshrkinkg();

                }
            });
    })

    function tshrkinkg() {
        $('#tshark').each(function(){
            var filename=$(this).data('filename');
            $.ajax({
                url: "tshark.php",
                type: "POST",
                data: { filename:filename }
            })
                .done(function( data ) {
                    if ( console && console.log ) {
                        $("#tshark").html(data);

                    }
                });
            //alert(filename);
        })
    }


    function setfilter() {


        $(".history-search").click(function(){

            var number=$("#telnumb").val();
            var date=$("#datesr").val();
            var caller=$("#caller").val();
            var called=$("#called").val();
            var callerip=$("#callerip").val();
            var calledip=$("#calledip").val();
            var timefrom=$("#timefrom").val();
            var timeto=$("#timeto").val();
            $("#fulllist").html('loading...');

            $.ajax({
                url: "list.php",
                type: "POST",
                data: { caller: caller, called: called, callerip:callerip, calledip:calledip, date: date,timefrom:timefrom, timeto:timeto }
            })
                .done(function( data ) {
                    if ( console && console.log ) {
                        $("#fulllist").html(data);
                        setclicks();
                    }
                });
        })
    }

    function setclicks() {
        $(".caller").click(function(){
            var num=$(this).data('num');
            $("#caller").val(num);
        })
        $(".called").click(function(){
            var num=$(this).data('num');
            $("#called").val(num);
        })
        $(".callerip").click(function(){
            var num=$(this).data('num');
            $("#callerip").val(num);
        })
        $(".calledip").click(function(){
            var num=$(this).data('num');
            $("#calledip").val(num);
        })

    }


});

