# README #

VoipMonitor viewer - web interface for [voip monitor sniffer](https://github.com/voipmonitor/sniffer)
### What? ###

* View list of calls
* Tshark view pcap files
* GovnoCode

### Install ###

```
apt-get install php5 php5-memcache php5-mysql memcached tshark php5-xsl
git clone https://bitbucket.org/kilex/voipmonview.git
cd voipmonview
# edit config.php.example and rename it to config.php
# symlink to folder with pcap files (for downloading pcaps)
ln -s /opt/spool/voipmonitor/ pcap
echo 'CREATE TABLE `cb_ip_groups` (`id` int(11) NOT NULL AUTO_INCREMENT, `descr` text NOT NULL, `ip` text NOT NULL, `trunk` tinyint(4) DEFAULT '0', `note` text, PRIMARY KEY (`id`);' | mysql -uuser -ppassword voipmonitor
```

### Thanks ###
