<?php
/**
 * Created by PhpStorm.
 * User: kilex
 * Date: 07.05.15
 * Time: 12:31
 */

include_once('mysqlc.php');

include_once('config.php');

function tomin($time) {
    $sec = $time % 60;
    $time = floor($time / 60);
    $min = $time % 60;
    $time = floor($time / 60);
    return "$min:$sec";
}

$mysql_connection=new SafeMySQL($sqlopt);

$memd = new Memcache;
$memd->connect('localhost', 11211) or die ("Не могу подключиться к кешу");


$respidscache=$memd->get("respids");
if ($respidscache) {
    $responseids=$respidscache;
//    print "respids cache<br>";

}
else {
    $query="SELECT * FROM voipmonitor.cdr_sip_response;";
    $responseids=$mysql_connection->getInd("id",$query);
    $memd->set("respids",$responseids,false,600);
}

$ipgroupscache=$memd->get("ipgroups");
if ($ipgroupscache) {
    $ipgroups=$ipgroupscache;
//    print "ipscache<br>";
}
else {
    $query="SELECT * FROM voipmonitor.cb_ip_groups;";
    $ipgroups=$mysql_connection->getInd("ip",$query);
    $memd->set("ipgroups",$ipgroups,false,600);
}

//print_r($responseids);

$date=$_POST['date'];
$caller=$_POST['caller'];
$called=$_POST['called'];
$callerip=$_POST['callerip'];
$calledip=$_POST['calledip'];
$timefrom=$_POST['timefrom'];
$timeto=$_POST['timeto'];

if (!$date) {
    //die ('Дату выбираем обязательно!');
    $date=date("y:m:d");
}

$where='';

if ($caller) $where.=" AND cdr.caller LIKE '%$caller%'";
if ($called) $where.=" AND cdr.called LIKE '%$called%'";
if ($callerip) $where.=" AND cdr.sipcallerip = inet_aton('$callerip')";
if ($calledip) $where.=" AND cdr.sipcalledip = inet_aton('$calledip')";
if (!$timefrom) {
    $timefrom='00:00:00';
}
if (!$timeto) {
    $timeto='23:59:59';
}

//print "$date $caller $called $where <br>";

$query="select
  cdr.ID,
  cdr.calldate,
  cdr.duration,
  cdr.connect_duration,
  cdr.caller,
  cdr.called,
  cdr.payload,
  cdr.mos_min_mult10,
  cdr.delay_sum,
  lastSIPresponse_id,
  lastSIPresponseNum,
  inet_ntoa(cdr.sipcallerip) as callerip,
  inet_ntoa(cdr.sipcalledip) as calledip,
  cdr.whohanged,
  CONCAT(DATE_FORMAT(cdr.calldate,'%Y-%m-%d/%H/%i/SIP/'), cdr_next.fbasename,'.pcap') as filepath,
  CONCAT(DATE_FORMAT(cdr.calldate,'%Y-%m-%d/%H/%i/RTP/'), cdr_next.fbasename,'.pcap') as filepathrtp
  from cdr
  left join cdr_next on cdr.ID = cdr_next.cdr_ID
  where cdr.calldate between '$date $timefrom' and '$date $timeto' $where
  order by cdr.calldate desc
  limit 200;";

//print $query;

$querycache=$memd->get("SQL$date$where$timefrom$timeto");
if ($querycache) {
    $resultlist=$querycache;
    print "<span class='label label-info'>from cache!</span>";
}
else {
    $resultlist=$mysql_connection->getAll($query);
    $memd->set("SQL$date$where$timefrom$timeto",$resultlist,false,600);
//    print "not from cache (";
}




print '<table class="table table-striped table-condensed">';
print "<tr>

        <th>calldate</th>
        <th>duration/conn_dur</th>
        <th>caller</th>
        <th>called</th>
        <th>callerip</th>
        <th>calledip</th>
        <th>Codec</th>
        <th>lastresult</th>
        <th>whohanged</th>
        <th>delay/mos</th>
        <th>tshark</th>

    </tr>";
foreach ($resultlist as $row) {
    print "<tr>";

    print "<td>";
    print "$row[calldate]";
    print "</td>";

    print "<td>";
    $duration=tomin($row[duration]);
    $durationc=tomin($row[connect_duration]);
    print "$duration / $durationc";
    print "</td>";

    print "<td>";
    print "<span class='caller' data-num='$row[caller]'>$row[caller]</span>";
    print "</td>";

    print "<td>";
    print "<span class='called' data-num='$row[called]'>$row[called]</span>";
    print "</td>";

    print "<td>";
    print "<span class='callerip' data-num='$row[callerip]'>$row[callerip]</span>";
    if (isset($ipgroups[$row['callerip']])) {
        print " (".$ipgroups[$row['callerip']]['descr'].")";
    }
    print "</td>";

    print "<td>";
    print "<span class='calledip' data-num='$row[calledip]'>$row[calledip]</span>";
    if (isset($ipgroups[$row['calledip']])) {
        print " (".$ipgroups[$row['calledip']]['descr'].")";
    }
    print "</td>";

    print "<td>";
    if ($payloadtypes[$row['payload']]) $payload=$payloadtypes[$row['payload']];
    else $payload=$row['payload'];
    print "<span class='label label-success'>$payload</span>";

    print "</td>";

    print "<td>";
    if ($row[lastSIPresponseNum]==200) {
        $responseclass='success';
    }
    elseif ($row[lastSIPresponseNum]==487) {
        $responseclass='danger';
    }
    elseif ($row[lastSIPresponseNum]==486) {
        $responseclass='warning';
    }

    else {
        $responseclass='info';
    }
    print "<span class='label label-$responseclass'>";
    print $responseids[$row['lastSIPresponse_id']]['lastSIPresponse'];
    print "</span></td>";


    print "<td>";
    if (isset($row['whohanged'])){
        if ($row['whohanged']=='caller') {
            $hangupclass='warning';
        }
    elseif ($row['whohanged']=='callee') {
            $hangupclass='success';
        }
    }
    else {
        $hangupclass='primary';

        $row['whohanged']='';
//        $row['whohanged']=$responseids[$row['lastSIPresponse_id']]['lastSIPresponse'];
//        $row['whohanged']=$responseids[$row['lastSIPresponse_id']];
//        print_r($responseids[$row['lastSIPresponse_id']][lastSIPresponse]);
    }
    print "<span class='label label-$hangupclass'>$row[whohanged]</span>";
    print "</td>";


    print "<td><nobr>";
    if ($row['delay_sum']<1000) $delaystyle='success';
    elseif ($row['delay_sum']<10000) $delaystyle='warning';
    else $delaystyle='danger';
    print "<span class='label label-$delaystyle'>$row[delay_sum]</span>";

    $loss=$row['mos_min_mult10'];
    if ($loss>39) $rtcpstyle='success';
    elseif ($loss>29) $rtcpstyle='warning';
    else $rtcpstyle='danger';
    if ($loss) print "<span class='label label-$rtcpstyle'>$loss</span><br>";

    print "</nobr></td>";

    print "<td>";
    print "<!-- <a href='./pcap/$row[filepathrtp]'>RTP</a> <a href='./pcap/$row[filepath]'>SIP</a> --> <a class='btn btn-info btn-xs' href='?debug=$row[ID]'><span class='glyphicon glyphicon-asterisk'></span></a>";
    print "</td>";


    print "</tr>";

}
print "</table>";
